#!/bin/sh

# ~/.mrconfig.d/debian-openstack.sh

set -e

PROJECT="debian-openstack"
NAME="Daniel Baumann"
EMAIL="daniel.baumann@progress-linux.org"
GPG_KEY="94A6A9B2"

REPOSITORIES="$(ssh alioth.debian.org "find /srv/git.debian.org/git/openstack -type d -name '*.git' | sed -e 's|^/srv/git.debian.org/git/openstack/||' | sort")"

cat > ${PROJECT}.conf << EOF
# mrconfig
EOF

for REPOSITORY in ${REPOSITORIES}
do
	DIRECTORY="$(dirname ${REPOSITORY})/"

	case "${DIRECTORY}" in
		old*|users*)
			continue
			;;
	esac

	REPOSITORY="$(basename ${REPOSITORY} .git)"

	case "${REPOSITORY}" in
		openstack)
			continue
			;;
	esac

cat >> ${PROJECT}.conf << EOF

[/srv/sources/${PROJECT}/${DIRECTORY}${REPOSITORY}]
checkout = git clone ssh://git.debian.org/git/openstack/${DIRECTORY}${REPOSITORY}.git &&
	cd ${REPOSITORY} && git checkout-all &&
	git config user.name "${NAME}" &&
	git config user.email "${EMAIL}" &&
	git config user.signingKey "${GPG_KEY}"
EOF

done
