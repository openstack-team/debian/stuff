How to use mr to clone all repositories at once
===============================================

1. sudo apt install --yes myrepos

2. git clone git://anonscm.debian.org/openstack/debian/stuff.git
   cp stuff/myrepos/git-checkout-all /usr/local/bin
   cp stuff/myrepos/mrconfig ~/.mrconfig
   mkdir ~/.mrconfig.d
   cp stuff/myrepos/debian-openstack.sh ~/.mrconfig.d

3. cd ~/.mrconfig.d
   Edit debian-openstack.sh with your name, email, and key.
   Use a location where to store all your git repositories, e.g. ~/sources.
   Run debian-openstack.sh, it will produce debian-openstack.conf

4. cd ~/sources && mr -j5 clone
