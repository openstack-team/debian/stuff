#!/bin/sh

set -e
#set -x

# Values from OpenStack
. ./config.conf

provision_vm () {
	. ${CREDENTIALS_FILE}

	echo -n "===> Searching for OpenStack VM... "
	if VM_ID=$(openstack server show ${HOSTNAME} -c id --format value 2>&1) ; then
		if [ "${REPROVISION}" = "yes" ] ; then
			echo -n "found: ${VM_ID}, rebuilding... "
			openstack server rebuild --image ${IMAGE_NAME} ${VM_ID} >/dev/null 2>&1
			echo "done."
		else
			echo "found: ${VM_ID}."
		fi
	else
	        echo -n "not found: creating... "
	        if ! VM_ID=$(openstack server create --image ${IMAGE_NAME} --flavor ${FLAVOR_NAME} --nic net-id=${NETWORK_NAME} --key-name ${SSH_KEY_NAME} ${HOSTNAME} -c id --format value 2>&1) ; then
	                echo "Could not create server... :("
	                exit 1
	        else
	                echo "created server: ${VM_ID}"
	        fi
	fi
}

wait_for_server_active () {
	echo -n "===> Waiting for server to be in ACTIVE state... "
	CNT=10
	while [ $(openstack server show -f value -c status ${VM_ID}) != "ACTIVE" ] && [ "${CNT}" != 0 ] ; do
		CNT=$(( ${CNT} - 1))
		sleep 1
	done
	echo "done."
}

find_ip_address () {
	echo -n "===> Searching ip address... "
	IFACE=$(openstack server show ${VM_ID} -c addresses --format value)
	SSH_HOST=$(echo ${IFACE} | cut -d= -f2)
	echo "found: ${SSH_HOST}: writing Ansible inventory file."
	echo "[osbpo]
${SSH_HOST}" >osbpo-inventory.ini
}

wait_for_ssh () {
	echo -n "===> Waiting for SSH to be up... "
	local COUNT CYCLES OTCI_CAN_SSH
	COUNT=120
	CYCLES=0
	OTCI_CAN_SSH=no

	ssh-keygen -f ~/.ssh/known_hosts -R ${SSH_HOST} >/dev/null 2>&1 || true
	while [ "${OTCI_CAN_SSH}" != "yes" ] && [ ${COUNT} != 0 ] ; do
		if ssh -o "StrictHostKeyChecking no" -o "ConnectTimeout 2" ${SYSUSERNAME}@${SSH_HOST} 'echo -n ""' >/dev/null 2>&1 ; then
			OTCI_CAN_SSH=yes
		else
			COUNT=$(( ${COUNT} - 1 ))
			CYCLES=$(( ${CYCLES} + 1 ))
			sleep 1
		fi
	done
	ssh-keygen -f ~/.ssh/known_hosts -R ${SSH_HOST} >/dev/null 2>&1 || true
	ssh -o "StrictHostKeyChecking no" -o "ConnectTimeout 2" ${SYSUSERNAME}@${SSH_HOST} 'echo -n ""' >/dev/null 2>&1 
	echo -n "SSH is up: setting key for root user... "
	ssh ${SYSUSERNAME}@${SSH_HOST} "sudo cp /home/debian/.ssh/authorized_keys /root/.ssh/authorized_keys"
	echo "done."
}

provision_virtual_machine () {
	provision_vm
	wait_for_server_active
	find_ip_address
	wait_for_ssh
}

get_roles_from_galaxy () {
	echo "===> Getting ansible roles..."
	if ! [ -d roles ] ; then
	        mkdir roles
	fi
	if ! [ -d roles/zabbix_agent_infomaniak ] ; then
	        ansible-galaxy install --force -r ansible-role-requirements.yml -p roles/
	fi
}

run_ansible_playbook () {
	echo "===> Running ansible ..."
	ansible-playbook osbpo.yml -i osbpo-inventory.ini -u root
}

#provision_virtual_machine
#get_roles_from_galaxy
run_ansible_playbook
