#!/bin/sh

set -e

if [ ! -e debian/control ]
then
	echo "$(basename ${PWD}): not a debian package"
	exit 0
fi

wrap-and-sort -bast

if [ -n "$(git diff)" ]
then
	dch "Running wrap-and-sort -bast."
	git commit -a -s -S -m "Running wrap-and-sort -bast."
fi
