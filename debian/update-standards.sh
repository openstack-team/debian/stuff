#!/bin/sh

set -e

VERSION="${1}"

if [ -z "${VERSION}" ]
then
	echo "Usage: ${0} STANDARDS_VERSION"
	exit 1
fi

if [ ! -e debian/control ]
then
	echo "$(basename ${PWD}): not a debian package"
	exit 0
fi

sed -i	-e "s|^\(Standards-Version:\) .*|\1 ${VERSION}|" \
debian/control

if [ -n "$(git diff)" ]
then
	dch "Updating standards version to ${VERSION}."
	git commit -a -s -S -m "Updating standards version to ${VERSION}."
fi
