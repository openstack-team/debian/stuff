#!/bin/sh

set -e

MESSAGE="${1}"

if [ -z "${MESSAGE}" ]
then
	echo "Usage: ${0} \"MESSAGE\""
	exit 1
fi

if [ ! -e debian/control ]
then
	echo "$(basename ${PWD}): not a debian package"
	exit 0
fi

if [ -n "$(git diff)" ]
then
	dch "${MESSAGE}"
	git commit -a -s -S -m "${MESSAGE}"
fi
