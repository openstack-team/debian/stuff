#!/bin/sh

set -e

STRING="$(git config --get remote.origin.url  | cut -d / -f 5-)"

if [ ! -e debian/control ]
then
	echo "$(basename ${PWD}): not a debian package"
	exit 0
fi

sed -i	-e "s|^\(Vcs-Browser:\) .*|\1 https://anonscm.debian.org/cgit/${STRING}|" \
	-e "s|^\(Vcs-Git:\) .*|\1 https://anonscm.debian.org/git/${STRING}|" \
debian/control

if [ -n "$(git diff)" ]
then
	dch "Updating vcs fields."
	git commit -a -s -S -m "Updating vcs fields."
fi
