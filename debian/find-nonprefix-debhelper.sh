#!/bin/sh

set -e

if [ ! -e debian/control ]
then
	echo "$(basename ${PWD}): not a debian package"
	exit 0
fi

cd debian
find . -maxdepth 1 -mindepth 1 -type f -and -not -name "*.*" | grep -Ev '(changelog|compat|control|copyright|rules|watch)'
cd "${OLDPWD}"
