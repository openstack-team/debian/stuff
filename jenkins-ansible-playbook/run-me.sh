#!/bin/sh

set -e
set +x

# Values for bookworm-bobcat
VM_ID=6c2c9c88-45f7-4e85-b1a7-54c36732f3af
IM_ID=633302be-bae0-410c-b1c5-0fc8f8a12215
HOST_IP=185.125.24.244

# Values for bookworm-bobcat
#VM_ID=807563c7-c2d2-4bc0-a35d-f2a8fd4f79e0
#IM_ID=522da4bf-06e6-40df-901f-8dc8e1a460b3
#HOST_IP=185.125.24.100

# Values for bookworm-antelope
#VM_ID=b913de2f-8728-4713-a542-16927bef3e1e
#IM_ID=1f00f222-0f7f-4e67-a813-cb21e7afa552
#HOST_IP=185.125.24.166

# Values for bookworm-zed
#VM_ID=d179f109-654d-4fd9-af87-edc226c85732
#IM_ID=1f00f222-0f7f-4e67-a813-cb21e7afa552
#HOST_IP=185.125.24.127

# Values for bullseye-yoga
#VM_ID=e68acb0f-024f-4db3-b341-23d2138d06d3
#IM_ID=232fe13b-25b7-453b-a124-41e79a2efd44
#HOST_IP=185.125.24.208

#VM_ID=3d5c79d5-29d3-4c27-a25e-a45c7b4864fc
#IM_ID=25870d7d-efd0-4fbc-94e1-55280a27bb0c
#HOST_IP=185.125.24.78

# Values for bullseye-xena
#VM_ID=6e2eb0b7-3923-489f-ac57-52aba56b1b85
#IM_ID=a630ac70-0654-44a4-9d7c-dfe656fc360b
#HOST_IP=128.65.194.9

# Values for bullseye-wallaby
#VM_ID=aba3d523-ab78-47da-85b3-79326f0a796b
#IM_ID=d723be2d-5363-44f4-8e31-f7a65535d1cf
#HOST_IP=185.125.24.243

# Values for bullseye-victoria
#VM_ID=62cee103-107f-40d4-b53c-57ebba67b126
#IM_ID=633302be-bae0-410c-b1c5-0fc8f8a12215
#HOST_IP=185.125.24.202

# Values for buster-ussuri
#VM_ID=30c4faa7-5db6-4fbd-bbb6-af11dbaff7b7
#IM_ID=debian-9.12.1-20200328-openstack-amd64.qcow2
#HOST_IP=185.125.24.56

# Values for buster-rocky
#VM_ID=87dfd2a4-28e4-4d69-ab52-27a26082bf04
#IM_ID=debian-9.9.6-20190815-openstack-amd64.qcow2
#HOST_IP=185.125.24.17

# Values for stretch-pike
#VM_ID=0040b36e-0b56-4512-9aac-ab975b7a0f8d
#IM_ID=debian-9.9.6-20190815-openstack-amd64.qcow2
#HOST_IP=185.125.24.54

# Values for stretch-queens
#VM_ID=3f657727-7924-4dd3-a8d4-5fe80ba19d67
#IM_ID=ac6eb940-aee1-488d-bf7d-b7dd8fa7c6fe
#HOST_IP=185.125.24.26

# Values for buster-stein
#VM_ID=ea19b949-6b52-414d-87b1-307e66b098c8
#IM_ID=8a5d0cea-bd84-47e7-947a-f19a6984558e
#HOST_IP=185.125.24.28

# Values for buster-train
#VM_ID=b9b6b100-d491-4468-9353-150056d5e2fb
#IM_ID=eaf47572-343f-4e9b-961d-328256bb348e
#HOST_IP=185.125.24.66

SYSUSERNAME=debian

wait_for_ssh () {
	local COUNT CYCLES OTCI_CAN_SSH SSH_HOST
	COUNT=120
	CYCLES=0
	OTCI_CAN_SSH=no
	SSH_HOST=${1}
	ssh-keygen -f ~/.ssh/known_hosts -R ${SSH_HOST} || true
	while [ "${OTCI_CAN_SSH}" != "yes" ] && [ ${COUNT} != 0 ] ; do
		if ssh -o "StrictHostKeyChecking no" -o "ConnectTimeout 2" ${SYSUSERNAME}@${SSH_HOST} 'echo -n ""' ; then
			OTCI_CAN_SSH=yes
		else
			COUNT=$(( ${COUNT} - 1 ))
			CYCLES=$(( ${CYCLES} + 1 ))
			sleep 1
		fi
	done
	ssh-keygen -f ~/.ssh/known_hosts -R ${SSH_HOST} || true
	ssh -o "StrictHostKeyChecking no" -o "ConnectTimeout 2" ${SYSUSERNAME}@${SSH_HOST} 'echo -n ""'
}


reprovision_vm () {
	set -x
	openstack server rebuild --image ${IM_ID} ${VM_ID}
	CNT=10
	while [ $(openstack server show -f value -c status ${VM_ID}) != "ACTIVE" ] && [ "${CNT}" != 0 ] ; do
		CNT=$(( ${CNT} - 1))
		sleep 1
	done
	wait_for_ssh ${HOST_IP}
	ssh ${SYSUSERNAME}@${HOST_IP} "sudo cp /home/debian/.ssh/authorized_keys /root/.ssh/authorized_keys"
}

if ! [ -d roles ] ; then
	mkdir roles
fi
if ! [ -d roles/zabbix_agent_infomaniak ] ; then
	ansible-galaxy install --force -r ansible-role-requirements.yml -p roles/
fi

#reprovision_vm
#ssh ${SYSUSERNAME}@${HOST_IP} "sudo cp /home/debian/.ssh/authorized_keys /root/.ssh/authorized_keys"
ansible-playbook jenkins-pkgos.yml -i jenkins-machine-inventory.ini -u root
