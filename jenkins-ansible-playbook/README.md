# Automatic Jenkins build machine for OpenStack backports

This ansible playbook will setup a Jenkins build machine.

## How to use
## 1. Setup a VM on OpenStack

You need at least 8 GB of RAM, 16 is better.

## Setup debian.net domain

Edit a debian.net-dns-config.asc with the following (adapt to your own
CNAME if you need to):

``` # cat debian.net-dns-config
stretch-queens in cname stretch-queens.infomaniak.ch.
stretch-rocky in cname stretch-rocky.infomaniak.ch.
buster-stein in cname buster-stein.infomaniak.ch.
# gpg --clearsign debian.net-dns-config
```

The message must be signed with your GPG key as per above, and send the
resulting file as the body of a message to changes@db.debian.org.
For example:

gpg --clearsign debian.net-dns-config
scp debian.net-dns-config.asc master.debian.org:
ssh master.debian.org "cat debian.net-dns-config.asc | mail changes@db.debian.org"

## Edit run-me.sh

Set variables:

```
VM_ID=7dbd9cae-8f2e-40c9-a93a-9dbff01d351e
IM_ID=8a5d0cea-bd84-47e7-947a-f19a6984558e
HOST_IP=128.65.194.13
SYSUSERNAME=debian
```

VM_ID is the id of your virtual machine in your OpenStack cloud.

IM_ID is the Debian base image to use.

HOST_IP is the ip address of your provisionned Debian VM.

SYSUSERNAME is the username to use when doing the first ssh.

## Fill-up jenkins-machine-inventory.ini

Setup the variables:

```
debianrelease=buster
openstackrelease=stein
domainname=debian.net
jenkinslogin=zigo
jenkinspass=XXXXXX
```

## Configure postinst.sh

Set the HOST_IP variable:

```
HOST_IP=185.125.24.26
```

## Run the playbook and configure Jenkins

```
./run-me.sh
```

Once the playbook has ran, you shall be able to browse:

https://buster-stein.debian.net

Continue the setup on the web, as directed by Jenkins. Then
install the IRC, AnsiColor and Matrix plugins at:

https://buster-stein.debian.net/pluginManager/

Then configure the security using the Matrix auth in here:

https://buster-stein.debian.net/configureSecurity/

click on "Matrix-based security" and select the tick box
for "Authenticated Users", and let Anonymous the rights
to build.

Go here: https://buster-stein.debian.net/configure

and configure the IRC: click on "Enable IRC Notification",
set the Hostname to "irc.debian.org", the port to 6667,
clic "Add" to add the #debian-openstack-commits channel,
choose buster-stein in front of Nickname, select "Use /notice command"
and "Use colors".

## Add jobs in your jenkins

This is what the postinst.sh does. To use it, you need to
go in ../salsa-scripts and run ./list-all-git, which will
list all of the git repository that Jenkins may build.

Note: to be able to run the postinst script, one *MUST*
disable the "Prevent Cross Site Request Forgery exploits"
option under global security.
