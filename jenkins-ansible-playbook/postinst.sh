#!/bin/sh

set -e
set +x

# bookworm-caracal
HOST_IP=185.125.24.244
# bookworm-bobcat
#HOST_IP=185.125.24.100
# bookworm-antelope
#HOST_IP=185.125.24.166
# bullseye-zed
#HOST_IP=185.125.24.208
# bullseye-yoga
#HOST_IP=185.125.24.78
# bullseye-xena
#HOST_IP=128.65.194.9
# buster-victoria:
#HOST_IP=185.125.24.202
# buster-victoria:
#HOST_IP=185.125.24.200
# buster-ussuri:
#HOST_IP=185.125.24.56
# buster-rocky:
#HOST_IP=185.125.24.17
# stretch-pike:
#HOST_IP=185.125.24.54
# stretch-queens:
#HOST_IP=185.125.24.26
# buster-stein:
#HOST_IP=128.65.194.13
# buster-train:
#HOST_IP=185.125.24.66

echo "===> Generating jobs.yaml.j2"
SECRET_TOKEN=$(openssl rand -hex 32)
TMP_JOBS=$(mktemp -t $(basename $0).XXXXXX)

echo "" >templates/jobs.yaml.j2
for i in $(cat ../salsa-scripts/all-git) ; do
	SUBGROUP=$(echo $i | cut -d/ -f2)
	PROJECT=$(echo $i | cut -d/ -f3 | cut -d, -f1 | sed s/.git//)
	echo "- job:
    name: ${PROJECT}
    builders:
        - shell: 'pkgos-bop-jenkins "${PROJECT} ${SUBGROUP}"'
    auth-token: g5rjtpms5emw
    logrotate:
        numToKeep: 4
    publishers:
        - ircbot:
            strategy: all
            notify-start: true
            message-type: summary
    wrappers:
        - ansicolor:
            colormap: xterm
    triggers:
        - gitlab:
            trigger-push: true
            secret-token: ${SECRET_TOKEN}
" >>${TMP_JOBS}
done

echo "Generated..."
echo "===> Uploading job file"
scp ${TMP_JOBS} root@${HOST_IP}:/root/jenkins-job.yaml
rm ${TMP_JOBS}

echo "Please run 'jenkins-jobs update jenkins-job.yaml' on ${HOST_IP}
after setting-up /etc/jenkins_jobs/jenkins_jobs.ini like this:

[job_builder]
ignore_cache=True
keep_descriptions=False
include_path=/usr/local/bin
recursive=False
allow_duplicates=False

[jenkins]
user=zigo
password=PASSWORD_GOES_HERE
url=https://bullseye-xena.debian.net/
"
exit 0

echo "===> Running Ansible playbook"
#ansible-playbook install-jenkins-jobs.yml -i jenkins-machine-inventory.ini -u root
