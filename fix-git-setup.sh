#!/bin/sh

echo -n "Fixing repos: "

cd ..
for DIR in /git/openstack/*/*.git ; do
	cd "$DIR"

	NAME=${DIR%.git}
	NAME=${NAME##*/}
	GROUP=${DIR%/*}
	GROUP=${GROUP##*/}

	GIT_BROWSER="https://anonscm.debian.org/cgit/openstack/$GROUP/$NAME.git/commit/?h=\${branch}?id=\${commit}"

	echo -n "$GROUP / $NAME "

	# Description
	DESC=$(cat description)
	if [ -z "$DESC" ] || [ "$DESC" = "Unnamed repository; edit this file 'description' to name the repository." ] ; then
		echo "Packaging $NAME" > description
	fi

	# Shared repository
	git config core.sharedRepository group

	# Deny non-FF
	git config receive.denyNonFastforwards true

	# KGB config
	git config kgb.conf /home/groups/openstack/kgb-client.conf
	git config kgb.web-link "$GIT_BROWSER"

	# Mail
	git config multimailhook.mailinglist "dispatch@tracker.debian.org"
	git config multimailhook.announceShortLog true
	git config multimailhook.emailDomain users.alioth.debian.org
	git config multimailhook.emailMaxLines 1000
        git config multimailhook.maxCommitEmails 20

	# Git hook post-receive
	echo "/home/groups/openstack/stuff/git-post-receive '$NAME'" >hooks/post-receive
	chmod 0770 hooks/post-receive || true
done

echo "...done!"
