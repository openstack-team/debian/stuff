#!/bin/sh

set -e

GROUP=$1
PACKAGE=$2

if [ -z "$GROUP" ] || [ -z "$PACKAGE" ] ; then
        echo "$0 <group> <package>"
        echo ""
        echo "Example: $0 python python-pika"
        exit
fi

cd "/git/openstack/$GROUP"
git init --bare --shared=group "$PACKAGE.git"
