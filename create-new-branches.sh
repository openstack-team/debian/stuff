#!/bin/sh

BRANCH=$1

if [ -z "$BRANCH" ] ; then
	echo "$0 <branch_name>"
	echo ""
	echo "Example: $0 debian/pike"
	exit
fi

for i in /git/openstack/*/*.git ; do
	echo $i
        cd $i
	git branch "$BRANCH"
	git symbolic-ref HEAD "refs/heads/$BRANCH"
done
