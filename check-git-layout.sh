#!/bin/sh

BRANCH=debian/pike

for i in /git/openstack/*/*.git/ ; do
	cd $i
	(git branch | grep "$BRANCH" >/dev/null) || echo $i
done
